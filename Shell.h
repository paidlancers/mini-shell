#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <sys/types.h> 
#include <sys/wait.h> 
#include <readline/readline.h> 

#define MAXCOM 1000 
#define MAXLIST 100
#define clear() printf("\033[H\033[J") 

void executeBackgroundProcess(char *cmd) {
	printf("pid: %d cmd : %s\n", getppid(), cmd);

	int a = system(cmd);

	if (a == 0) {
		printf("pid %d done\n", getppid());
	}
}

int readCommand(char* str) { 
	char* buffer;
	char cwd[1024];
	
	getcwd(cwd, sizeof(cwd));
	printf("SWS:%s:", getenv("USER"));
	printf("%s", cwd);
	buffer = readline(">"); 
	
	int len = strlen(buffer);

	if (strlen(buffer) != 0) { 
		if(buffer[len - 1] == '&') {
			executeBackgroundProcess(buffer);

			return 2;
		} else {
			strcpy(str, buffer);

			return 0;
		}
	} else { 
		return 1; 
	} 
}

void execArgs(char** parsed) { 
	pid_t pid = fork();

	if (pid == -1) { 
		printf("\nFailed forking child.."); 
		return; 
	} else if (pid == 0) { 
		execvp(parsed[0], parsed);
		exit(0); 
	} else { 
		wait(NULL); 
		return; 
	} 
} 


int ownCmdHandler(char** parsed) { 
	int internalCommandsCnt = 2, i, ownCommandIndex = 0; 
	char* internalCommands[internalCommandsCnt]; 
	char* username; 

	internalCommands[0] = "exit"; 
	internalCommands[1] = "cd"; 

	for (i = 0; i < internalCommandsCnt; i++) {
		if (strcmp(internalCommands[i], parsed[0]) == 0) {
			ownCommandIndex = i + 1;
			break;
		}
	}

	if (ownCommandIndex == 1) {
		printf("\nGoodbye\n");
		exit(0);
	} else if (ownCommandIndex == 2) {
		chdir(parsed[1]);
		return 1;
	}

	return 0; 
} 

void parseSpace(char* str, char** parsed) {
	char *temp;
	int i = 0;

	temp = strtok(str," ");

	while(temp != NULL) {
		parsed[i] = temp;
		i++;
		temp = strtok(NULL, " ");
	}

	parsed[i] = NULL;
}

int processString(char* str, char** parsed) {
	parseSpace(str, parsed);

	return ownCmdHandler(parsed) ? 0 : 1;
} 
#include "shell.h"

int main() {
	char input[MAXCOM], *parsedArguments[MAXLIST];

	while (1) {
		char temp_cmd[MAXLIST];
		int flag = readCommand(input);

		if (flag == 2) {
			continue;
		}

		if (processString(input, parsedArguments) == 1) {
			execArgs(parsedArguments);
		}
	}

	return 0;
} 

CC=gcc
CFLAGS=-I.
DEPS = sws.c

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

run: sws.c
	$(CC) sws.c -lreadline